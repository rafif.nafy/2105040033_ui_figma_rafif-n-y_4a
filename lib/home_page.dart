import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('J-travel'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Find your next trip',
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16.0),
            // Fitur Search dan Filter
            Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: 'Search',
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                SizedBox(width: 16.0),
                ElevatedButton(
                  child: Text('Filter'),
                  onPressed: () {
                    // Implementasi logika filter
                  },
                ),
              ],
            ),
            SizedBox(height: 32.0),
            // Fitur Rekomendasi: Popular Location by Price
            Text(
              'Popular Location by Price',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              height: 200.0, // Atur sesuai kebutuhan
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  // Daftar rekomendasi dengan gambar
                  _buildRecommendationCard(
                    imageUrl:
                        'https://terasmalioboro.jogjaprov.go.id/wp-content/uploads/2022/08/5ee208425be9b.jpg',
                    title: 'Yogyakarta',
                  ),
                  _buildRecommendationCard(
                    imageUrl:
                        'https://jakarta-tourism.sgp1.cdn.digitaloceanspaces.com/images/article/8437415341664213099.jpg',
                    title: 'Jakarta',
                  ),
                  _buildRecommendationCard(
                    imageUrl:
                        'https://asset.kompas.com/crops/8vfJFkHa95q-Jl6-URjkWNOcXRA=/0x0:1200x800/750x500/data/photo/2020/09/19/5f65a842d8d53.jpg',
                    title: 'Magelang',
                  ),
                  // Tambahkan rekomendasi lainnya di sini
                ],
              ),
            ),
            SizedBox(height: 32.0),
            // Fitur Rekomendasi: Locations by Popularity
            Text(
              'Locations by Popularity',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              height: 200.0, // Atur sesuai kebutuhan
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  // Daftar rekomendasi dengan gambar
                  _buildRecommendationCard(
                    imageUrl:
                        'https://heritage.kai.id/media/cover%2013.jpg?1505617606123',
                    title: 'Museum',
                  ),
                  _buildRecommendationCard(
                    imageUrl:
                        'https://asset.kompas.com/crops/POnyEveRYNGK9KtGt5Qb-_npsKg=/0x0:1200x800/750x500/data/photo/2021/04/09/606fbf79879f2.jpg',
                    title: 'Bahari',
                  ),
                  _buildRecommendationCard(
                    imageUrl:
                        'https://mytrip123.com/wp-content/uploads/2022/02/curug-silawe.jpg',
                    title: 'Alam',
                  ),
                  // Tambahkan rekomendasi lainnya di sini
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blueAccent,
        selectedItemColor: Colors.blueGrey,
        unselectedItemColor: Colors.blueAccent,
        selectedLabelStyle: TextStyle(color: Colors.blueGrey),
        unselectedLabelStyle: TextStyle(color: Colors.blueAccent),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Wishlist',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Guide',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Account',
          ),
        ],
      ),
    );
  }

  Widget _buildRecommendationCard(
      {required String imageUrl, required String title}) {
    return Container(
      margin: EdgeInsets.only(right: 16.0),
      width: 160.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.network(
              imageUrl,
              height: 120.0,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 8.0),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
